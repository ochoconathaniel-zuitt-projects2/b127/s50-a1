import { Fragment, useState , useEffect } from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
//pages
import Home from './pages/Home';
import Courses from './pages/Courses'
import Counter from './components/Counter'
import Register from './pages/Register'
import Login from './pages/Login'
import { BrowserRouter as Router } from 'react-router-dom';
import {Route,Switch} from 'react-router-dom';
import Error from './pages/Error'
//Bootstrap
import Logout from './pages/Logout'
import { Container } from 'react-bootstrap';
import SpecificCourse from './pages/SpecificCourse'



import {UserProvider} from './UserContext';

function App() {
 
  const[user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  const unsetUser = () => {

      localStorage.clear()
  }

    useEffect(() => {
      console.log(user);
      console.log(localStorage)

    }, [user])



  return (
   <UserProvider value={ {user,setUser,unsetUser} }>
    <Router>
      < AppNavbar />
      <Container>
       <Switch>
        
        
        < Route exact path="/" component={Home} />
        < Route exact path="/courses" component={Courses} />
        < Route exact path="/courses/:courseId" component={SpecificCourse} />
        < Route exact path="/register" component={Register} />
        < Route exact path="/login" component={Login} />
        < Route exact path="/logout" component={Logout} />
        < Route component={Error} />
      </Switch>
      </Container>
    </Router>
  </UserProvider>
  );
}

export default App;
