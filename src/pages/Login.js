

import {Fragment, useState, useEffect, useContext} from 'react';
import { Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';
import { Redirect, useHistory } from 'react-router-dom';

export default function Login() {

		const history = useHistory();

		const {user,setUser} = useContext(UserContext)

		const [email,checkEmail] = useState('');
		const [password,checkPassword] = useState('');


		const [isActive, setIsActive] = useState(false);


		

		useEffect(() => {

			if(email !== '' && password !== ''){

				setIsActive(true);
			}

			else {

				setIsActive(false)
			}





		},[email,password])
	

		function loginUser(e){

			e.preventDefault();

		fetch('http://localhost:4000/users/login', {

			method: 'POST',
			headers: {

				'Content-Type': 'application/json'
			},

			body:JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {
				console.log(data)
				if(data.accessToken !== undefined){
						localStorage.setItem('accessToken', data.accessToken);
						setUser({accessToken: data.accessToken});


						Swal.fire({

						title: 'yay!',
						icon: 'success',
						text: 'thank you for logging in!'


					})

						fetch('http://localhost:4000/users/details', {
							headers: {
								Authorization: `Bearer ${data.accessToken}`
							}
						})
						.then(res => res.json())
						.then(data => {

							console.log(data)
							
							if(data.isAdmin === true ){
								localStorage.setItem('email', data.email)
								localStorage.setItem('isAdmin', data.isAdmin)
								setUser({
									email: data.email,
									isAdmin: data.isAdmin


								})

									history.push('/courses')

							} else {

									history.push('/')


							}


						})




				}else {

					Swal.fire({

						title: 'oops!',
						icon: 'error',
						text: 'something went wrong'


					})


				}

					checkEmail('')
					checkPassword('')


		})

	}


		return(
			(user.accessToken !== null) ?
			<Redirect to ="/"/>
			:
		<Fragment>
		<h1 class="mt-5">Login</h1>
		<Form onSubmit ={(e) => loginUser(e)}>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => checkEmail(e.target.value)}
					required

				/>
				
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify your Password"
						value={password}
						onChange={e => checkPassword(e.target.value)}
						required

						/>	
					</Form.Group>
					
						
						{isActive ?
						<Button variant="dark" type="submit" id="submitBtn">Submit</Button>
						:
						<Button variant="secondary" type="submit" id="submitBtn" disabled>Submit</Button>
						}
					</Form>
					</Fragment>







			)

	}