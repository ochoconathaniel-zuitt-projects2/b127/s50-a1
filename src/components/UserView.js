import { useState, useEffect, Fragment} from 'react';
import CourseCard from './CourseCard';

export default function UserView ({ coursesData }){

	const [courses,setCourses] = useState([])


	useEffect(()=> {

			const courseArr = coursesData.map(course => {

					if(course.isActive === true){

						return(
							< CourseCard courseProp={course} key={course._id}/>

							)

					} else {

						return null;
					}




			})


				setCourses(courseArr)


	},[coursesData])

  	return(
  		<Fragment>

  			{courses}

  		</Fragment>	


  		)


}